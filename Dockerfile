FROM ubuntu:jammy

LABEL org.opencontainers.image.authors="Lolo_Bee"


ENV DEBIAN_FRONTEND noninteractive

RUN apt update \
&& apt -y install software-properties-common \
&& add-apt-repository -y ppa:ondrej/php \
&& apt install --yes \
cron curl \
wget \
ca-certificates \
jq \
iputils-ping \
zlib1g-dev libpng-dev libicu-dev libldap2-dev libzip-dev libbz2-dev \
apache2 php7.4 php7.4-curl php7.4-zip php7.4-gd php7.4-intl php7.4-intl php7.4-imagick php7.4-bz2 php7.4-imap php7.4-memcache php7.4-pspell php7.4-tidy php7.4-xmlrpc php7.4-xsl php7.4-mbstring php7.4-ldap php7.4-apcu libapache2-mod-php7.4 php7.4-mysql mariadb-server


COPY glpi-start.sh /opt/
RUN chmod +x /opt/glpi-start.sh
ENTRYPOINT ["/opt/glpi-start.sh"]

EXPOSE 80 443
